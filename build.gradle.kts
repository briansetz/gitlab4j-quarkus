plugins {
    kotlin("jvm") version "1.9.0"
}


allprojects {
    group = "org.example"
    version = "1.0-SNAPSHOT"

    apply {
        plugin("org.jetbrains.kotlin.jvm")
    }

    repositories {
        mavenCentral()
    }

    dependencies {
//        implementation("org.gitlab4j:gitlab4j-api:5.3.0") // This does not work!
        implementation("org.gitlab4j:gitlab4j-api:6.0.0-rc.2")
        testImplementation(kotlin("test"))
    }

    tasks.test {
        useJUnitPlatform()
    }

    kotlin {
        jvmToolchain(17)
    }
}
