package nl.rug.digitallab.gitlab.standalone

import org.gitlab4j.api.GitLabApi

fun main(args: Array<String>) {
    val personalAccessToken = System.getenv("GITLAB_PERSONAL_ACCESS_TOKEN")
        ?: throw IllegalStateException("GITLAB_PERSONAL_ACCESS_TOKEN environment variable not set")

    val gitlab = GitLabApi("https://gitlab.com", personalAccessToken)

    gitlab.userApi.getUser("briansetz").let {
        println("User: ${it.username} (${it.name})")
    }
}