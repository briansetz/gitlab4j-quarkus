package nl.rug.digitallab.gitlab.quarkus.repository

import io.quarkus.hibernate.reactive.panache.PanacheRepositoryBase
import io.smallrye.mutiny.Uni
import jakarta.enterprise.context.ApplicationScoped
import nl.rug.digitallab.gitlab.quarkus.entity.Submission
import nl.rug.digitallab.gitlab.quarkus.entity.SubmissionStatus
import java.util.*

@ApplicationScoped
class SubmissionMetaRepository: PanacheRepositoryBase<Submission, UUID> {
    /**
     * Update the status of a [Submission] to a [SubmissionStatus].
     *
     * @param submission The submission to update.
     * @param status The new status of the submission.
     *
     * @return The number of rows updated.
     */
    fun updateStatus(submission: Submission, status: SubmissionStatus): Uni<Submission> {
        submission.status = status
        return persist(submission)
    }

    /**
     * Find all submissions, optionally filtered by [CourseId], [GroupId], and/or [AssignmentId].
     *
     * @param courseId The id of the course to filter by.
     * @param groupId The id of the group to filter by.
     * @param assignmentId The id of the assignment to filter by.
     *
     * @return A list of (filtered) submissions.
     */
    fun find(courseId: UUID?, assignmentId: UUID?, groupId: UUID?): Uni<List<Submission>> {
        return find("""
                (cast(?1 AS uuid) IS null OR courseId     = ?1) AND
                (cast(?2 AS uuid) IS null OR groupId      = ?2) AND 
                (cast(?3 AS uuid) IS null OR assignmentId = ?3)
            """, courseId, groupId, assignmentId)
            .list()
    }
}