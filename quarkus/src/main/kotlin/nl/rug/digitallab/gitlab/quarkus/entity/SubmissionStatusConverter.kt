package nl.rug.digitallab.gitlab.quarkus.entity

import jakarta.persistence.AttributeConverter

/**
 * Converts a [SubmissionStatus] to and from an integer. This is necessary because JPA does not know how to serialize
 * and deserialize [SubmissionStatus] enum values with a custom field. By default, JPA uses the ordinal value of the
 * enum, which is not what we want.
 */
class SubmissionStatusConverter: AttributeConverter<SubmissionStatus, Int> {
    override fun convertToDatabaseColumn(attribute: SubmissionStatus): Int {
        return attribute.code
    }

    override fun convertToEntityAttribute(dbData: Int): SubmissionStatus? {
        return SubmissionStatus.entries.find { it.code == dbData }
    }
}