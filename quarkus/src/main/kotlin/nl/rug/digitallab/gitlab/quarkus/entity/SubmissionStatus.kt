package nl.rug.digitallab.gitlab.quarkus.entity

/**
 * Represents the status of a submission.
 *
 * @param code The code associated with the status.
 */
enum class SubmissionStatus(val code: Int) {
    // Set by submission service
    FAILED(-10),
    PREPARING(10),
    UPLOADING(20),
    QUEUEING(30),
    QUEUED(40),
}