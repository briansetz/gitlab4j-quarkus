package nl.rug.digitallab.gitlab.quarkus

import io.smallrye.mutiny.Uni
import jakarta.inject.Inject
import jakarta.ws.rs.GET
import jakarta.ws.rs.Path
import nl.rug.digitallab.gitlab.quarkus.entity.Submission
import nl.rug.digitallab.gitlab.quarkus.entity.SubmissionStatus
import nl.rug.digitallab.gitlab.quarkus.repository.SubmissionMetaRepository
import org.gitlab4j.api.GitLabApi
import java.util.UUID

@Path("/api/v1/gitlab")
class QuarkusResource {
    @Inject
    lateinit var submissionMetaRepository: SubmissionMetaRepository


    @GET
    @Path("/user/{username}")
    fun getUserDetails(username: String): Uni<String> {
        val personalAccessToken = System.getenv("GITLAB_PERSONAL_ACCESS_TOKEN")
            ?: throw IllegalStateException("GITLAB_PERSONAL_ACCESS_TOKEN environment variable not set")

        val gitlab = GitLabApi("https://gitlab.com", personalAccessToken)

        val user = gitlab.userApi.getUser("briansetz")

        return submissionMetaRepository.persist(Submission().apply {
            submissionId = UUID.randomUUID()
            courseId = UUID.randomUUID()
            groupId = UUID.randomUUID()
            assignmentId = UUID.randomUUID()
            profile = "test"
            status = SubmissionStatus.PREPARING
        }).map { user.name }
    }
}