package nl.rug.digitallab.gitlab.quarkus.entity

import jakarta.persistence.Convert
import jakarta.persistence.Entity
import jakarta.persistence.Id
import jakarta.validation.constraints.NotBlank
import jakarta.validation.constraints.NotNull
import org.hibernate.annotations.CreationTimestamp
import org.hibernate.annotations.UpdateTimestamp
import java.time.Instant
import java.util.UUID


/**
 * Represents a submission of a student for an assignment.
 *
 * @property submissionId The ID of the submission.
 * @property created The time the submission was created.
 * @property updated The time the submission was last updated.
 * @property courseId The ID of the course the submission is for.
 * @property groupId The ID of the group that submitted the submission.
 * @property assignmentId The ID of the assignment the submission is for.
 * @property profile The profile that should be used to execute the submission.
 * @property status The status of the submission. Defaults to [SubmissionStatus.PREPARING].
 */
@Entity
class Submission {
    @Id
    @NotNull(message = "Submission ID cannot be null")
    lateinit var submissionId: UUID

    @CreationTimestamp
    lateinit var created: Instant

    @UpdateTimestamp
    lateinit var updated: Instant

    @NotNull(message = "Course ID cannot be null")
    lateinit var courseId: UUID

    @NotNull(message = "Group ID cannot be null")
    lateinit var groupId: UUID

    @NotNull(message = "Assignment ID cannot be null")
    lateinit var assignmentId: UUID

    @NotBlank(message = "Profile cannot be blank")
    lateinit var profile: String

    @Convert(converter = SubmissionStatusConverter::class)
    @NotNull
    lateinit var status: SubmissionStatus

    companion object {
        /**
         * Helper function to create a new submission.
         *
         * @param submissionId The ID of the submission.
         * @param courseId The ID of the course the submission is for.
         * @param groupId The ID of the group that submitted the submission.
         * @param assignmentId The ID of the assignment the submission is for.
         * @param profile The profile that should be used to execute the submission.
         *
         * @return The created submission.
         */
        fun create(
            submissionId: UUID,
            courseId: UUID,
            groupId: UUID,
            assignmentId: UUID,
            profile: String,
            submissionStatus: SubmissionStatus = SubmissionStatus.PREPARING,
        ): Submission {
            val submission = Submission()
            submission.submissionId = submissionId
            submission.courseId = courseId
            submission.groupId = groupId
            submission.assignmentId = assignmentId
            submission.profile = profile
            submission.status = submissionStatus
            return submission
        }
    }
}